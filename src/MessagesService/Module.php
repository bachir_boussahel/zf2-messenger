<?php

namespace MessagesService;

use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\ModuleEvent;
use Zend\Stdlib\ArrayUtils;
use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;

class Module implements ConfigProviderInterface, ConsoleUsageProviderInterface
{
    public function init(ModuleManager $moduleManager)
    {
        $events = $moduleManager->getEventManager();
        $events->attach(ModuleEvent::EVENT_MERGE_CONFIG, array($this, 'addRhubbitMessagesModules'));
    }

    public function addRhubbitMessagesModules(EventInterface $e)
    {
        $moduleManager = $e->getTarget();
        $loadedModules = $moduleManager->getLoadedModules();

        $modules = $moduleManager->getModules();
        $configListener = $e->getConfigListener();
        $config = $configListener->getMergedConfig(false);

        $rhubbitModules = $this->getConfig()['zf2-messenger_modules'];
        foreach ($rhubbitModules as $rhubbitModule) {
            if (!array_key_exists($rhubbitModule, $loadedModules)) {
                // Loading rhubbit messages modules not presents in main config 'modules'
                $moduleManager->loadModule($rhubbitModule);
                $modules[] = $rhubbitModule;

                $module = $moduleManager->getModule($rhubbitModule);
                if (($module instanceof ConfigProviderInterface) || (is_callable([$module, 'getConfig']))) {
                    // Merge the config
                    $moduleConfig = $module->getConfig();
                    $config = ArrayUtils::merge($config, $moduleConfig);
                }
            }
        }

        $moduleManager->setModules($modules);
        $configListener->setMergedConfig($config);
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'message send <type> [--limit=] [--debug|-d] [--verbose|-v] [--log|-l]' => "Command to send messages from related queue extracting the oldest",
            array('<type>', "message type to send, accept only 'email', 'sms' or 'push' string"),
            array('--limit', "number of messages extracted ('--limit' default value is 10)"),
            array('--debug|-d', "enable debug mode printing all params received in console"),
            array('--verbose|-v', "enable verbose mode printing send status log in console"),
            array('--log|-l', "enable log writing send status log in queue log file"),
            'message send <type> to <to> from <from> params <params> <msg> [<subject>] [--verbose|-v] [--log|-l]' => "Send a single message immediately",
            array('<type>', "message type to send, accept only 'email', 'sms' or 'push' string"),
            array('<to>', "message's recipient string"),
            array('<from>', "message's sender string"),
            array('<params>', "message's parameters string"),
            array('<msg>', "message's content string"),
            array('<subject>', "message's subject string"),
            array('--verbose|-v', "enable verbose mode printing send status log in console"),
            array('--log|-l', "enable log writing send status log in queue log file"),
            // Rhubbit messages install commands
            'zf2-messenger <command> [--verbose|-v] [--log|-l]' => "Command to install or update module:",
            array('<command>', "accept only 'install' or 'update' string"),
            array('--verbose|-v', "enable verbose mode printing send status log in console"),
            array('--log|-l', "enable log writing send status log in queue log file"),

        );
    }
}
