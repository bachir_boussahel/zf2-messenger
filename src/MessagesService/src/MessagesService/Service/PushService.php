<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 20/01/17
 * Time: 15:21
 */
namespace MessagesService\Service;

use MessagesService\Exception\MessagesServiceException,
    MessagesService\Exception\MessagesServicePushException;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Http\Client;
use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Adapter\Apns as ApnsAdapter,
    Sly\NotificationPusher\Adapter\Gcm as GcmAdapter,
    Sly\NotificationPusher\Collection\DeviceCollection,
    Sly\NotificationPusher\Model as PushModel,
    Sly\NotificationPusher\Model\Device,
    Sly\NotificationPusher\Model\Message as PushManagerMessage,
    Sly\NotificationPusher\Model\Push,
    Sly\NotificationPusher\Exception\PushException;

class PushService implements IMessagesService
{
    const PUSH_IOS = "ios";
    const PUSH_ANDROID = "android";

    private static $validPushType = array(
        self::PUSH_IOS,
        self::PUSH_ANDROID
    );

    private static $validModeType = array(
        PushManager::ENVIRONMENT_DEV,
        PushManager::ENVIRONMENT_PROD
    );

    private static $instance = null;
    private $config;
    private $type;

    protected $serviceLocator;
    protected $messagesModel = null;

    private function __construct(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $messagesModel)
    {
        $this->config = $config;
        $this->serviceLocator = $serviceLocator;
        $this->messagesModel = $messagesModel;
        $this->type = Factory::MODE_PUSH;
    }

    public static function getInstance(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $machineModel)
    {
        if (self::$instance == null) {
            self::$instance = new PushService($serviceLocator, $config, $machineModel);
        }
        return self::$instance;
    }

    public function getDefaultPushLimit()
    {
        return $this->config['limit.push.default'];
    }

    public function getMaxLimitsTries()
    {
        return $this->config['limit.max.tries'];
    }

    /**
     * Send a push message
     *
     * @param Message $message
     * @return bool
     * @throws MessagesServicePushException
     */
    public function sendNotificationPush(Message $message)
    {
        // Set custom sms parameters
        $params = json_decode($message->getOption('jsonParams'));
        if ($params !== null) {
            $adapter = null;
            $messageOptions = array();
            switch ($message->getOption('device')) {
                case self::PUSH_IOS:
                    $adapter = new ApnsAdapter(array(
                        'certificate' => $params->certificate,
                        'passPhrase' => property_exists($params, "passPhrase") ? $params->passPhrase : "",
                    ));
                    // iOS properties
                    property_exists($params, "badge") ? $messageOptions["badge"] = (int)$params->badge : false;
                    property_exists($params, "sound") ? $messageOptions["sound"] = (string)$params->sound : false;
                    property_exists($params, "launchImage") ? $messageOptions["launchImage"] = (string)$params->launchImage : false;
                    property_exists($params, "actionLocKey") ? $messageOptions["actionLocKey"] = (string)$params->actionLocKey : false;
                    property_exists($params, "locKey") ? $messageOptions["locKey"] = (string)$params->locKey : false;
                    property_exists($params, "locArgs") ? $messageOptions["locArgs"] = (array)explode(",", $params->locArgs) : false;

                    break;
                case self::PUSH_ANDROID:
                    $adapter = new GcmAdapter(array(
                        'apiKey' => $params->apiKey,
                    ));

                    $message->getOption('collapseId') != null ? $adapter->setParameter("collapseKey", (string)$message->getOption('collapseId')) : false;
                    property_exists($params, "restrictedPackageName") ? $adapter->setParameter("restrictedPackageName", (string)$params->restrictedPackageName) : false;
                    property_exists($params, "delayWhileIdle") ? $adapter->setParameter("delayWhileIdle", (bool)$params->delayWhileIdle) : false;
                    property_exists($params, "ttl") ? $adapter->setParameter("ttl", (int)$params->ttl) : false;
                    property_exists($params, "dryRun") ? $adapter->setParameter("dryRun", (bool)$params->dryRun) : false;
                    break;
            }

            // Common data
            $message->getOption('title') != null ? $messageOptions["title"] = (string)$message->getOption('title') : false;
            property_exists($params, "customData") ? $messageOptions["custom"] = (array)$params->customData : false;

            if ($message->getOption('trackingUrl') !== null) {
                $messageOptions["custom"]["tracking_url"] = $message->getOption('trackingUrl');
            }

            // Set the device(s) to push the notification to.
            $devices = new DeviceCollection(
                array(
                    new Device($message->getOption('to')),
                ));

            $pushMessage = new PushManagerMessage($message->getText(), $messageOptions);

            $push = new Push($adapter, $devices, $pushMessage);

            switch ($message->getOption('mode')) {
                case PushManager::ENVIRONMENT_DEV:
                    $pushManagerEnv = PushManager::ENVIRONMENT_DEV;
                    break;
                case PushManager::ENVIRONMENT_PROD:
                    $pushManagerEnv = PushManager::ENVIRONMENT_PROD;
                    break;
                default:
                    $pushManagerEnv = PushManager::ENVIRONMENT_DEV;
            }

            $pushManager = new PushManager($pushManagerEnv);
            $pushManager->add($push);
            try {
                $getParam = array(
                    "type" => $this->getType(),
                    "hash_id" => $message->getOption('hashId') !== null ? $message->getOption('hashId') : null,
                    "status" => "success");
                /* @var $devicesCollection DeviceCollection */
                $devicesCollection = $pushManager->push(); // Returns a collection of notified devices
                $iterator = $devicesCollection->getIterator();
                $res = true;
                foreach ($iterator as $pushModel) {
                    /* @var $pushModel PushModel */
                    if (!$pushModel->isPushed()) {
                        throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH, "Push notification not sent"), MessagesServiceException::ERROR_PUSH_ID);
                    }
                }

                if ($adapter instanceof ApnsAdapter) {
                    // The feedback service is used to list tokens of devices which not have your application anymore.
                    $feedback = $pushManager->getFeedback($adapter);
                    /**
                     * @var string $token
                     * @var \DateTime $value
                     */
                    foreach ($feedback as $token => $value) {
                        $res = "App removed from device on " . date("Y-m-d H:i:s", $value->getTimestamp());
                        $getParam["status"] = "failed";
                    }
                }
                if (property_exists($params, "callback")) {
                    $options = array(
                        'maxredirects' => 0,
                        'timeout' => 5,
                    );
                    if (strpos($params->callback, "https://") !== false) {
                        $options['sslcapath'] = $this->config['ssl_ca_path'];
                    }

                    $HttpClient = new Client();
                    $HttpClient->setUri($params->callback);
                    $HttpClient->setOptions($options);
                    $HttpClient->setMethod("GET");
                    $HttpClient->setParameterGet($getParam);
                    $HttpClient->send();
                    $HttpClient->getAdapter()->close();
                }
            } catch (PushException $ex) {
                throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH, $ex->getMessage()), MessagesServiceException::ERROR_PUSH_ID);
            }
        } else {
            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_JSON_VALIDATOR, MessagesServiceException::ERROR_PUSH_JSON_VALIDATOR_ID);
        }

        return $res;
    }

    /**
     * Send a Push message immediately
     *
     * @param $params
     * @param $from
     * @param $to
     * @param $message
     * @param string $subject
     * @param string $device
     * @param string $mode
     * @return bool
     * @throws MessagesServicePushException
     * @throws \Exception
     */
    public function sendMessage($params, $from, $to, $message, $subject = "", $device = "", $mode = PushManager::ENVIRONMENT_DEV)
    {
        $message = new Message($message);
        $message->setOption('jsonParams', $params);
        $message->setOption('device', $device);
        $message->setOption('to', $to);
        $message->setOption('mode', $mode);
        if ($subject !== "") {
            $message->setOption('title', $subject);
        }

        $res = $this->validateMessage($message);
        if ($res === true) {
            return $this->sendNotificationPush($message);
        }

        return $res;
    }

    /**
     * @param Message $message
     * @return bool
     * @throws MessagesServicePushException
     */
    public function validateMessage(Message $message)
    {
        // Mandatory params
        if (!$message->hasOption('device')) {
            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_DEVICE_MISSING, MessagesServiceException::ERROR_PUSH_DEVICE_MISSING_ID);
        } else {
            if (!$this->isValidPushType($message->getOption('device'))) {
                $modes = implode("|", self::$validPushType);
                throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH_DEVICE_VALIDATOR, $modes), MessagesServiceException::ERROR_PUSH_DEVICE_VALIDATOR_ID);
            } else {
                $device = $message->getOption('device');
            }
        }
        if (!$message->hasOption('jsonParams')) {
            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_JSON_MISSING, MessagesServiceException::ERROR_PUSH_JSON_MISSING_ID);
        } else {
            $jsonParams = json_decode($message->getOption('jsonParams'));
            if ($jsonParams === null) {
                throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_JSON_VALIDATOR, MessagesServiceException::ERROR_PUSH_JSON_VALIDATOR_ID);
            } else {
                switch ($device) {
                    case self::PUSH_IOS:
                        // Params for iOS device must have 'certificate' key
                        if (!property_exists($jsonParams, "certificate")) {
                            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_CERTIFICATE_MISSING, MessagesServiceException::ERROR_PUSH_CERTIFICATE_MISSING_ID);
                        }
                        break;
                    case self::PUSH_ANDROID:
                        // Params for Android device must have 'apiKey'
                        if (!property_exists($jsonParams, "apiKey")) {
                            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_APIKEY_MISSING, MessagesServiceException::ERROR_PUSH_APIKEY_MISSING_ID);
                        }
                        break;
                }
            }
        }
        if (!$message->hasOption('to')) {
            throw new MessagesServicePushException(MessagesServiceException::MESSAGE_PUSH_TO_MISSING, MessagesServiceException::ERROR_PUSH_TO_MISSING_ID);
        }

        // Optional params
        if (!$message->hasOption('title')) {
            $message->setOption('title', null);
        }
        if (!$message->hasOption('mode')) {
            $message->setOption('mode', PushManager::ENVIRONMENT_DEV);
        } else {
            if (!$this->isValidModeType($message->getOption('mode'))) {
                $modes = implode("|", self::$validModeType);
                throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH_MODE_VALIDATOR, $modes), MessagesServiceException::ERROR_PUSH_MODE_VALIDATOR_ID);
            }
        }
        if (!$message->hasOption('collapseId')) {
            $message->setOption('collapseId', null);
        }
        if (!$message->hasOption('sendDate')) {
            $message->setOption('sendDate', date("Y-m-d H:i:s"));
        }
        if (!$message->hasOption('trackingUrl')) {
            $message->setOption('trackingUrl', null);
        }

        return true;
    }

    /**
     * @param Message $message
     * @return bool|string
     * @throws MessagesServicePushException
     */
    public function pullMessage(Message $message)
    {
        return $this->messagesModel->pullNotificationPushMessage($message);
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServicePushException
     */
    public function pullMessages(array $messages)
    {
        return $this->messagesModel->pullNotificationPushMessages($messages);
    }

    public function pushMessages($limit)
    {
        return $this->messagesModel->pushNotificationPushMessages($limit);
    }

    public function removeMessageFromQueue($hashId)
    {
        return $this->messagesModel->removePushMessageFromQueue($hashId);
    }

    public function removeMessagesFromQueue($hashIds)
    {
        return $this->messagesModel->removePushMessagesFromQueue($hashIds);
    }

    public function getLogPattern()
    {
        return $this->config['log_file'];
    }

    public function getType()
    {
        return $this->type;
    }

    public function getFeedback()
    {
        return $this->feedback;
    }

    public function checkParams($params, $to, $from, $msg, $subject = "")
    {
        $check = true;
        $jsonParams = json_decode($params);

        if ($jsonParams === null) {
            $msg = "PUSH parameters are not valid.\n" .
                "Must be a JSON string. For example:\n" .
                "{\"badge\": 2, \"sound\": \"example.aiff\"}\n";
            return $msg;
        }

        if ($to == "") {
            return "PUSH recipient is empty.\n";
        }


        return $check;
    }

    private function isValidPushType($type)
    {
        return in_array(strtolower($type), self::$validPushType);
    }

    private function isValidModeType($type)
    {
        return in_array(strtolower($type), self::$validModeType);
    }

    public function updateMessage($hashId, Message $message)
    {
        return $this->messagesModel->updatePushMessage($hashId, $message);
    }

    /**
     * @param array $hashIds
     * @param string $message
     * @param string $sendDate
     * @param string $subject
     * @return bool|string
     * @throws MessagesServicePushException
     */
    public function updateMessages(array $hashIds, $message = "", $sendDate = "", $subject = "")
    {
        return $this->messagesModel->updatePushMessages($hashIds, $message, $subject, $sendDate);
    }
}