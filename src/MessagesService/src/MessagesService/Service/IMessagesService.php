<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 13/01/17
 * Time: 16:21
 */

namespace MessagesService\Service;

use MessagesService\Model\MessagesModel;
use MessagesService\Model\MessageModel as Message;
use Zend\ServiceManager\ServiceLocatorInterface;

interface IMessagesService
{
    public static function getInstance(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $machineModel);

    public function getDefaultPushLimit();

    public function getMaxLimitsTries();

    public function sendMessage($params, $from, $to, $message, $subject = "");

    public function validateMessage(Message $message);

    public function pullMessage(Message $message);

    public function pullMessages(array $messages);

    public function updateMessage($hashId, Message $message);

    public function updateMessages(array $hashIds, $message = "", $sendDate = "", $subject = "");

    public function pushMessages($limit);

    public function removeMessageFromQueue($hashId);

    public function removeMessagesFromQueue($hashIds);

    public function getLogPattern();

    public function getType();
}