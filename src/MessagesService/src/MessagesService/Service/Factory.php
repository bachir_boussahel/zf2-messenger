<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 13/01/17
 * Time: 15:55
 */

namespace MessagesService\Service;

use MessagesService\Model\MessagesModel;
use Zend\ServiceManager\ServiceLocatorInterface;
use Exception;

class Factory
{
    const MODE_SMS = "sms";
    const MODE_MAIL = "email";
    const MODE_PUSH = "push";

    private static $service = null;
    private static $validMsgType = array(
        self::MODE_MAIL,
        self::MODE_SMS,
        self::MODE_PUSH
    );

    /**
     * Return message service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $serviceType
     * @param MessagesModel $messagesModel
     * @return EmailService|PushService|SmsService|null
     * @throws Exception
     */
    public static function create(ServiceLocatorInterface $serviceLocator, $serviceType, MessagesModel $messagesModel)
    {
        $config = $serviceLocator->get('config');

        if (self::isValidMessageType($serviceType)) {
            switch (strtolower($serviceType)) {
                case self::MODE_SMS:
                    if ((self::$service instanceof SmsService) == false) {
                        self::$service = SmsService::getInstance($serviceLocator, $config['sms_message'], $messagesModel);
                    }
                    return self::$service;
                    break;
                case self::MODE_MAIL:
                    if ((self::$service instanceof EmailService) == false) {
                        self::$service = EmailService::getInstance($serviceLocator, $config['email_message'], $messagesModel);
                    }
                    return self::$service;
                    break;
                case self::MODE_PUSH:
                    if ((self::$service instanceof PushService) == false) {
                        self::$service = PushService::getInstance($serviceLocator, $config['push_message'], $messagesModel);
                    }
                    return self::$service;
                    break;
            }
        }

        throw new Exception("Service " . $serviceType . " not exist");
    }

    private static function isValidMessageType($type)
    {
        return in_array(strtolower($type), self::$validMsgType);
    }
}