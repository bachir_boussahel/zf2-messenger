<?php
namespace MessagesService\Service;

use MessagesService\Exception\MessagesServiceException,
    MessagesService\Exception\MessagesServiceSmsException;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message;
use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;
use Zend\ServiceManager\ServiceLocatorInterface;

class SmsService implements IMessagesService
{
    private static $instance = null;
    private $config;
    private $smsApiLists;
    private $type;

    protected $serviceLocator;
    protected $messagesModel = null;

    /**
     * SmsService constructor.
     * @param $serviceLocator
     * @param $config
     * @param MessagesModel $messagesModel
     */
    private function __construct(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $messagesModel)
    {
        $this->config = $config;
        $this->smsApiLists = $serviceLocator->get('config')['sms_api'];
        $this->serviceLocator = $serviceLocator;
        $this->messagesModel = $messagesModel;
        $this->type = Factory::MODE_SMS;
    }

    /**
     * @param $serviceLocator
     * @param MessagesModel $config
     * @param MessagesModel $machineModel
     * @return SmsService|null
     */
    public static function getInstance(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $machineModel)
    {
        if (self::$instance == null) {
            self::$instance = new SmsService($serviceLocator, $config, $machineModel);
        }
        return self::$instance;
    }

    public function getDefaultPushLimit()
    {
        return $this->config['limit.push.default'];
    }

    public function getMaxLimitsTries()
    {
        return $this->config['limit.max.tries'];
    }

    public function getPhoneCountries()
    {
        return $this->smsApiLists['phone_country'];
    }

    /**
     * Send an SMS
     *
     * @param Message $message
     * @return bool|SMSApi\Api\Response\MessageResponse
     * @throws MessagesServiceSmsException
     */
    public function sendSms(Message $message)
    {
        // Set custom sms parameters
        $params = json_decode($message->getOption('jsonParams'));
        $user = $params->username;
        $pwd = $params->password;

        $client = new Client($user);
        $client->setPasswordHash($pwd);

        $smsapi = new SmsFactory();
        $smsapi->setClient($client);

        try {
            /* @var $actionSend \SMSApi\Api\Action\Sms\Send */
            $actionSend = $smsapi->actionSend();

            $actionSend->setSender($message->getOption('from'));
            $actionSend->setTo($message->getOption('to'));
            $actionSend->setText($message->getText());

            $response = $actionSend->execute();

            /** @var SMSApi\Api\Response\MessageResponse $status */
            foreach ($response->getList() as $status) {
                $res = $status;
            }
            if (property_exists($params, "callback")) {
                $getParam = array(
                    "type" => $this->getType(),
                    "hash_id" => $message->hasOption("hashId") ? $message->getOption("hashId") : null,
                    "status" => "success");

                $HttpClient = new Client();
                $HttpClient->setUri($params->callback);
                $HttpClient->setOptions(array(
                    'maxredirects' => 0,
                    'timeout' => 5,));
                $HttpClient->setMethod("GET");
                $HttpClient->setParameterGet($getParam);
                $HttpClient->send();
                $HttpClient->getAdapter()->close();
            }
        } catch (SmsapiException $ex) {
            $msg = sprintf(MessagesServiceException::MESSAGE_SMS, $ex->getMessage());
            throw new MessagesServiceSmsException($msg, MessagesServiceException::ERROR_SMS_ID);
        }

        return $res;
    }

    /**
     * Send an SMS immediately
     *
     * @param $params
     * @param $from
     * @param $to
     * @param $messageText
     * @param string $subject
     * @return bool|SMSApi\Api\Response\MessageResponse
     * @throws MessagesServiceSmsException
     * @throws \Exception
     */
    public function sendMessage($params, $from, $to, $messageText, $subject = "")
    {
        $message = new Message($messageText);
        $message->setOption('jsonParams', $params);
        $message->setOption('from', $from);
        $message->setOption('to', $to);

        $res = $this->validateMessage($message);
        if ($res === true) {
            return $this->sendSms($message);
        }
    }

    /**
     * Validate an SMS message
     *
     * @param Message $message
     * @return bool
     * @throws MessagesServiceSmsException
     */
    public function validateMessage(Message $message)
    {
        // Mandatory params
        if (!$message->hasOption('jsonParams')) {
            throw new MessagesServiceSmsException(MessagesServiceException::MESSAGE_SMS_JSON_MISSING, MessagesServiceException::ERROR_SMS_JSON_MISSING_ID);
        } else {
            $jsonParams = json_decode($message->getOption('jsonParams'));
            if ($jsonParams === null) {
                // Set params from application config
                $smsApiParams = new \stdClass();
                $smsApiParams->username = $this->getSmsapiUser();
                $smsApiParams->password = $this->getSmsapiPwd();
                $message->setOption('jsonParams', json_encode($smsApiParams));
            } elseif (
                !property_exists($jsonParams, 'username') ||
                !property_exists($jsonParams, 'password')
            ) {
                throw new MessagesServiceSmsException(MessagesServiceException::MESSAGE_SMS_JSON_VALIDATOR, MessagesServiceException::ERROR_SMS_JSON_VALIDATOR_ID);
            }
        }
        if (!$message->hasOption('from')) {
            throw new MessagesServiceSmsException(MessagesServiceException::MESSAGE_SMS_FROM_MISSING, MessagesServiceException::ERROR_SMS_FROM_MISSING_ID);
        }
        if (!$message->hasOption('to')) {
            throw new MessagesServiceSmsException(MessagesServiceException::MESSAGE_SMS_TO_MISSING, MessagesServiceException::ERROR_SMS_TO_MISSING_ID);
        } else {
            $toNumber = $message->getOption('to');
            if (!is_numeric($toNumber)) {
                throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS_TO_VALIDATOR, $toNumber), MessagesServiceException::ERROR_SMS_TO_VALIDATOR_ID);
            }
        }

        // Optional params
        if (!$message->hasOption('sendDate')) {
            $message->setOption('sendDate', date("Y-m-d H:i:s"));
        }

        return true;
    }

    /**
     * @param Message $message
     * @return bool|string
     * @throws MessagesServiceSmsException
     */
    public function pullMessage(Message $message)
    {
        return $this->messagesModel->pullSmsMessage($message);
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServiceSmsException
     */
    public function pullMessages(array $messages)
    {
        return $this->messagesModel->pullSmsMessages($messages);
    }

    /**
     * Send SMSs using queue stored in a DB
     *
     * @param $limit
     * @return bool|string
     */
    public function pushMessages($limit)
    {
        return $this->messagesModel->pushSmsMessages($limit);
    }

    public function removeMessageFromQueue($hashId)
    {
        return $this->messagesModel->removeSmsFromQueue($hashId);
    }

    public function removeMessagesFromQueue($hashIds)
    {
        return $this->messagesModel->removeSmssFromQueue($hashIds);
    }

    public function getLogPattern()
    {
        return $this->config['log_file'];
    }

    public function getSmsapiStatusMessage($status)
    {
        return $this->smsApiLists['smsapi.status.list'][$status];
    }

    public function getDefaultSmsSender()
    {
        return $this->config['sender.sms'];
    }

    public function getType()
    {
        return $this->type;
    }

    private function getSmsapiUser()
    {
        return $this->config['smsapi.user'];
    }

    private function getSmsapiPwd()
    {
        return $this->config['smsapi.password'];
    }

    public function updateMessage($hashId, Message $message)
    {
        return $this->messagesModel->updateSmsMessage($hashId, $message);
    }

    /**
     * @param array $hashIds
     * @param string $message
     * @param string $sendDate
     * @param string $subject
     * @return bool|string
     * @throws MessagesServiceSmsException
     */
    public function updateMessages(array $hashIds, $message = "", $sendDate = "", $subject = "")
    {
        return $this->messagesModel->updateSmsMessages($hashIds, $message, $sendDate);
    }
}
