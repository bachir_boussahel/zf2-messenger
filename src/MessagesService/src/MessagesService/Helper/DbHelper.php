<?php
namespace MessagesService\Helper;

use Zend\ServiceManager\ServiceLocatorInterface;

class DbHelper
{
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param $dsn
     * @param $usr
     * @param $pwd
     * @return \PDO
     */
    public function getDatabase($dsn, $usr, $pwd)
    {
        $db = new \PDO($dsn, $usr, $pwd);

        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $db->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);

        return $db;
    }

    /**
     * @return \PDO
     */
    public function getCoreDatabase()
    {
        $config = $this->serviceLocator->get('config');

        $dsn = $config['database']['core']['dsn'];
        $usr = $config['database']['core']['usr'];
        $pwd = $config['database']['core']['pwd'];

        return $this->getDatabase($dsn, $usr, $pwd);
    }

}