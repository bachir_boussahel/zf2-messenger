<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 18/01/17
 * Time: 15:02
 */
namespace MessagesService\Model;

use MessagesService\Helper\AbstractLogger;
use MessagesService\Helper\DbHelper;
use Zend\ServiceManager\ServiceLocatorInterface;

class InstallModel extends AbstractLogger
{
    private static $sqlCreateQueueMailTable = "CREATE TABLE IF NOT EXISTS `queue_mail` (
                                                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                      `hash_id` varchar(32) DEFAULT NULL,
                                                      `status` smallint(2) NOT NULL DEFAULT '0' COMMENT 'READY, SENDING, FAILED, RETRY',
                                                      `num_tries` int(11) DEFAULT NULL,
                                                      `type` enum('email') NOT NULL DEFAULT 'email',
                                                      `params` text NOT NULL,
                                                      `template_layout` varchar(255) DEFAULT NULL,
                                                      `template_params` text,
                                                      `log_message` varchar(255) NOT NULL DEFAULT '',
                                                      `from` varchar(255) DEFAULT NULL,
                                                      `from_alias` varchar(255) DEFAULT NULL,
                                                      `reply_to` varchar(255) DEFAULT NULL,
                                                      `reply_to_alias` varchar(255) DEFAULT NULL,
                                                      `to` varchar(255) DEFAULT NULL,
                                                      `subject` varchar(255) DEFAULT NULL,
                                                      `content` text,
                                                      `tracking_url` varchar(255) DEFAULT NULL,
                                                      `send_date` datetime DEFAULT NULL,
                                                      `created_at` datetime DEFAULT NULL,
                                                      `updated_at` datetime DEFAULT NULL,
                                                      `deleted_at` datetime DEFAULT NULL,
                                                      PRIMARY KEY (`id`)
                                                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    private static $sqlCreateQueueSmsTable = "CREATE TABLE IF NOT EXISTS `queue_sms` (
                                                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                      `hash_id` varchar(32) DEFAULT NULL,
                                                      `status` smallint(2) NOT NULL DEFAULT '0' COMMENT 'READY, SENDING, FAILED, RETRY',
                                                      `num_tries` int(11) DEFAULT NULL,
                                                      `type` enum('sms') DEFAULT 'sms',
                                                      `params` text NOT NULL,
                                                      `log_message` varchar(255) NOT NULL DEFAULT '',
                                                      `from` varchar(255) DEFAULT NULL,
                                                      `from_alias` varchar(255) DEFAULT NULL,
                                                      `to` varchar(255) DEFAULT NULL,
                                                      `content` text,
                                                      `callback_id` varchar(255) DEFAULT NULL,
                                                      `send_date` datetime DEFAULT NULL,
                                                      `created_at` datetime DEFAULT NULL,
                                                      `updated_at` datetime DEFAULT NULL,
                                                      `deleted_at` datetime DEFAULT NULL,
                                                      PRIMARY KEY (`id`)
                                                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    private static $sqlCreateQueueNotificationPushTable = "CREATE TABLE IF NOT EXISTS `queue_push` (
                                                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                              `hash_id` varchar(32) DEFAULT NULL,
                                                              `status` smallint(2) NOT NULL DEFAULT '0',
                                                              `num_tries` int(11) NOT NULL DEFAULT '0',
                                                              `type` enum('ios','android') NOT NULL DEFAULT 'ios',
                                                              `params` text NOT NULL,
                                                              `log_message` varchar(255) NOT NULL DEFAULT '',
                                                              `mode` enum('dev','prod') NOT NULL DEFAULT 'dev',
                                                              `title` varchar(255) DEFAULT NULL,
                                                              `to` varchar(255) DEFAULT NULL,
                                                              `content` text NOT NULL,
                                                              `tracking_url` varchar(255) DEFAULT NULL,
                                                              `collapse_id` varchar(255) DEFAULT NULL,
                                                              `send_date` datetime DEFAULT NULL,
                                                              `created_at` datetime DEFAULT NULL,
                                                              `updated_at` datetime DEFAULT NULL,
                                                              `deleted_at` datetime DEFAULT NULL,
                                                              PRIMARY KEY (`id`)
                                                          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    private static $sqlMailQueueTableUpdate_1 = "ALTER TABLE `queue_mail` ADD `hash_id` VARCHAR(32)  NULL  DEFAULT NULL  AFTER `id`;";
    private static $sqlSmsQueueTableUpdate_1 = "ALTER TABLE `queue_sms` ADD `hash_id` VARCHAR(32)  NULL  DEFAULT NULL  AFTER `id`;";
    private static $sqlPushQueueTableUpdate_1 = "ALTER TABLE `queue_push` ADD `hash_id` VARCHAR(32)  NULL  DEFAULT NULL  AFTER `id`;";

    private static $sqlMailQueueTableUpdate_2 = "ALTER TABLE `queue_mail` ADD `send_date` datetime  NULL  AFTER `content`;";
    private static $sqlSmsQueueTableUpdate_2 = "ALTER TABLE `queue_sms` ADD `send_date` datetime  NULL  AFTER `callback_id`;";
    private static $sqlPushQueueTableUpdate_2 = "ALTER TABLE `queue_push` ADD `send_date` datetime  NULL  AFTER `collapse_id`;";

    private static $sqlMailQueueTableUpdateSendDate_2 = "UPDATE `queue_mail` SET `send_date` = `created_at`;";
    private static $sqlSmsQueueTableUpdateSendDate_2 = "UPDATE `queue_sms` SET `send_date` = `created_at`;";
    private static $sqlPushQueueTableUpdateSendDate_2 = "UPDATE `queue_push` SET `send_date` = `created_at`;";

    private static $sqlMailQueueTableUpdate_3_0 = "ALTER TABLE `queue_mail` ADD `template_params` TEXT  NULL  AFTER `template_layout`;";
    private static $sqlMailQueueTableUpdate_3_1 = "ALTER TABLE `queue_mail` ADD `tracking_url` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `content`;";
    private static $sqlPushQueueTableUpdate_3 = "ALTER TABLE `queue_push` ADD `tracking_url` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `content`;";


    private static $sqlMailQueueTableUpdateStatus_4 = "ALTER TABLE `queue_mail` CHANGE `status` `status` SMALLINT(2)  NOT NULL  DEFAULT '0';";
    private static $sqlSmsQueueTableUpdateStatus_4 = "ALTER TABLE `queue_sms` CHANGE `status` `status` SMALLINT(2)  NOT NULL  DEFAULT '0';";
    private static $sqlPushQueueTableUpdateStatus_4 = "ALTER TABLE `queue_push` CHANGE `status` `status` SMALLINT(2)  NOT NULL  DEFAULT '0';";

    private static $sqlMailQueueTableUpdateStatus_5 = "ALTER TABLE `queue_mail` CHANGE `log_message` `log_message` varchar(255) NOT NULL DEFAULT '';";
    private static $sqlSmsQueueTableUpdateStatus_5 = "ALTER TABLE `queue_sms` CHANGE `log_message` `log_message` varchar(255) NOT NULL DEFAULT '';";
    private static $sqlPushQueueTableUpdateStatus_5 = "ALTER TABLE `queue_push` CHANGE `log_message` `log_message` varchar(255) NOT NULL DEFAULT '';";

    private static $sqlMailQueueTableUpdateTemplateParams_6 = "ALTER TABLE `queue_mail` CHANGE `template_params` `template_params` text;";

    private static $sqlMailQueueTableUpdateTemplateParams_7_0 = "ALTER TABLE `queue_mail` ADD `reply_to` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `from_alias`;";
    private static $sqlMailQueueTableUpdateTemplateParams_7_1 = "ALTER TABLE `queue_mail` ADD `reply_to_alias` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `reply_to`;";


    private $verboseMode = false;
    private $logMode = false;

    protected $dbHelper;
    protected $db;

    public function __construct(ServiceLocatorInterface $serviceLocator, $verboseMode, $logMode)
    {
        $this->dbHelper = new DbHelper($serviceLocator);
        $this->db = $this->dbHelper->getCoreDatabase();

        $this->verboseMode = $verboseMode;
        $this->logMode = $logMode;
    }

    public function createDbTables()
    {
        echo "Creating DB Tables...\n";
        try {
            $stmt = $this->db->prepare(self::$sqlCreateQueueMailTable);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlCreateQueueSmsTable);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlCreateQueueNotificationPushTable);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    public function updateDbTables()
    {
        $createRes = $this->createDbTables();

        // Update tables
        echo "Updating DB Tables...\n";
        $updRes_1 = $this->tableUpd_1();
        $updRes_2 = $this->tableUpd_2();
        $updRes_3 = $this->tableUpd_3();
        $updRes_4 = $this->tableUpd_4();
        $updRes_5 = $this->tableUpd_5();
        $updRes_6 = $this->tableUpd_6();
        $updRes_7 = $this->tableUpd_7();

        $res = $createRes &&
            $updRes_1 &&
            $updRes_2 &&
            $updRes_3 &&
            $updRes_4 &&
            $updRes_5 &&
            $updRes_6 &&
            $updRes_7;

        return $res;

    }

    private function tableUpd_1()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdate_1);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlSmsQueueTableUpdate_1);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdate_1);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_2()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdate_2);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateSendDate_2);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlSmsQueueTableUpdate_2);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlSmsQueueTableUpdateSendDate_2);
            if (!$stmt->execute()) {
                return false;
            };

            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdate_2);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdateSendDate_2);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_3()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdate_3_0);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdate_3_1);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdate_3);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_4()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateStatus_4);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlSmsQueueTableUpdateStatus_4);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdateStatus_4);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_5()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateStatus_5);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlSmsQueueTableUpdateStatus_5);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlPushQueueTableUpdateStatus_5);
            if (!$stmt->execute()) {
                return false;
            };
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_6()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateTemplateParams_6);
            if (!$stmt->execute()) {
                return false;
            };

        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    private function tableUpd_7()
    {
        try {
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateTemplateParams_7_0);
            if (!$stmt->execute()) {
                return false;
            };
            $stmt = $this->db->prepare(self::$sqlMailQueueTableUpdateTemplateParams_7_1);
            if (!$stmt->execute()) {
                return false;
            };

        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }

        return true;
    }

    /**
     * Return TRUE if logger will show log in console / page, else return FALSE
     * @return bool
     */
    public function showLog()
    {
        return $this->verboseMode;
    }

    /**
     * Return complete file path if logger will wrote log on file. else return NULL
     * @return null|string
     */
    public function getLogFile()
    {
        $logFile = null;
        if ($this->logMode) {
            $logFile = ini_get('error_log');
        }

        return $logFile;
    }
}