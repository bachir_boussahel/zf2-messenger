<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/02/17
 * Time: 10:46
 */

namespace MessagesService\Exception;

class MessagesServiceSmsException extends MessagesServiceException
{
    function __constructor($msg = null, $code = null) {
        $msg  = $msg === null ? MessagesServiceException::MESSAGE_SMS : $msg;
        $code = $code === null ? MessagesServiceException::ERROR_SMS_ID : $code;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return MessagesServiceException::MESSAGE_SMS;
    }
}