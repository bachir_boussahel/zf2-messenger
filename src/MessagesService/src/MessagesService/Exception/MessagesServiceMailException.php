<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/02/17
 * Time: 10:44
 */

namespace MessagesService\Exception;

class MessagesServiceMailException extends MessagesServiceException
{
    function __constructor($msg = null, $code = null) {
        $msg  = $msg === null ? MessagesServiceException::MESSAGE_MAIL_VALIDATOR : $msg;
        $code = $code === null ? MessagesServiceException::ERROR_MAIL_VALIDATOR_ID : $code;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return MessagesServiceException::MESSAGE_MAIL_VALIDATOR;
    }
}